import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
users;
usersKeys;
constructor(service:UsersService) {
  service.getUsers().subscribe(
    response=>{//console.log(response)//arrow function .json() converts the string that we recived to jason
    this.users= response.json();
    this.usersKeys = Object.keys(this.users);
 });
  
}
  ngOnInit() {
  }

}

