import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';


import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesService } from "./messages/messages.service";
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent  

  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    MessagesService,
     UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }