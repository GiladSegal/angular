import { Injectable } from '@angular/core';
import{Http} from '@angular/http';

@Injectable()
export class MessagesService {
  http:Http;//http -> שם התכונה. Http-> סוג התכונה
  getMessages(){
      //return ['Message1','Message2','Message3','Message4'];
    //get messages from the SLIM rest API(DONT say DB!)
    return this.http.get("http://localhost/slim/messages");


  }
  constructor(http:Http) {
    this.http = http;
   }

}