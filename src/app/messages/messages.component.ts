import { MessagesService } from './messages.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
 // messages = ['Message1','Message2','Message3','Message4'];
 messages;//תכונה ריקה בשביל שנוכל להשתמש בה בקונסטרקטור
 messagesKeys;
  constructor(service:MessagesService) {
    //let service = new MessagesService;--מחקנו כדי לאפשר יצירה אוטומטית מאנגולר
    service.getMessages().subscribe(
      response=>{//console.log(response)//arrow function .json() converts the string that we recived to jason
      this.messages= response.json();
      this.messagesKeys = Object.keys(this.messages);
   });
    
  }

  ngOnInit() {
  }

}